library ieee;
use ieee.std_logic_1164.all;

entity sitzsensor_tb is
end sitzsensor_tb;

architecture behave of sitzsensor_tb is

	signal clk : std_logic := '0';
	constant clk_period : time := 2 ns;

	component sitzsensor is
	port(
		CLOCK_50 	: in 		std_logic;
		KEY_R 		: in 		std_logic;
		
		GPIO_SCL 	: inout 	std_logic;
		GPIO_SDA 	: inout 	std_logic;

		GPIO_VIB	: out		std_logic;
		GPIO_INV	: out		std_logic;
		
		LED_R 		: out 	std_logic
	);
	end component sitzsensor;

begin
	
	sitzsensor_INST : sitzsensor
	port map(
		CLOCK_50 => clk,
		KEY_R => '1'
	);

	clk <= not clk after clk_period/2;

end behave;