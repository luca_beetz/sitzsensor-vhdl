library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sitzsensor is
	port(
		CLOCK_50 	: in 		std_logic;
		KEY_R 		: in 		std_logic;
		
		GPIO_SCL 	: inout 	std_logic;
		GPIO_SDA 	: inout 	std_logic;

		GPIO_VIB		: out		std_logic;
		GPIO_INV		: out		std_logic;
		
		LED_R 		: out 	std_logic
	);

end sitzsensor;

architecture arch of sitzsensor is

	component MPU6050
		port(
			MCLK 		: in 	std_logic;
			nRST 		: in 	std_logic;
			TIC 		: in 	std_logic;
			SRST		: out std_logic;
			DOUT		: out std_logic_vector(7 downto 0);
			RD			: out std_logic;
			WE			: out std_logic;
			QUEUED	: in	std_logic;
			NACK		: in	std_logic;
			STOP		: in 	std_logic;
			DATA_V	: in 	std_logic;
			DIN		: in 	std_logic_vector(7 downto 0);
			ADR		: out std_logic_vector(3 downto 0);
			DATA		: out std_logic_vector(7 downto 0);
			LOAD		: out std_logic;
			COMPLETED: out std_logic;
			RESCAN	: in 	std_logic
		);
	end component;
	
	component I2CMASTER
		generic(
			DEVICE	: std_logic_vector(7 downto 0)
		);
		
		port(
			MCLK		: in	std_logic;
			nRST		: in	std_logic;
			SRST		: in	std_logic;
			TIC		: in	std_logic;
			DIN		: in	std_logic_vector(7 downto 0);
			DOUT		: out	std_logic_vector(7 downto 0);
			RD			: in	std_logic;
			WE			: in	std_logic;
			NACK		: out	std_logic;
			QUEUED	: out	std_logic;
			DATA_V	: out	std_logic;
			STATUS	: out	std_logic_vector(2 downto 0);
			STOP		: out std_logic;
			SCL_IN	: in	std_logic;
			SCL_OUT	: out	std_logic;
			SDA_IN	: in	std_logic;
			SDA_OUT 	: out	std_logic
		);
	end component;

	-- Signal declarations --
	-- Two signals to store the values from the two sensors
	signal XREG_1		: std_logic_vector(7 downto 0);
	signal XREG_2		: std_logic_vector(7 downto 0);

	signal TIC			: std_logic;
	signal SRST			: std_logic;
	signal DOUT			: std_logic_vector(7 downto 0);
	signal RD			: std_logic;
	signal WE			: std_logic;
	signal QUEUED		: std_logic;
	signal NACK			: std_logic;
	signal STOP			: std_logic;
	signal DATA_V		: std_logic;
	signal DIN			: std_logic_vector(7 downto 0);
	signal ADR			: std_logic_vector(3 downto 0);
	signal DATA			: std_logic_vector(7 downto 0);
	signal LOAD			: std_logic;
	signal COMPLETED	: std_logic;
	signal RESCAN		: std_logic;
	signal STATUS		: std_logic_vector(2 downto 0); -- Useful for debugging, displays the current status of the FSM
	signal SCL_IN		: std_logic;
	signal SCL_OUT		: std_logic;
	signal SDA_IN		: std_logic;
	signal SDA_OUT 	: std_logic;

	signal counter		: std_logic_vector(7 downto 0);

	signal nRST			: std_ulogic;
	
	-- Required for changing the sensor address repeatedly
	signal c_sensor, c_sensor_nxt	: std_logic;

begin

	-- Inverted reset signal
	nRST <= KEY_R;
	
	-- The MPU6050 component used for communicating with the sensor
	I_MPU6050_0 : MPU6050
		port map (
			MCLK			=> CLOCK_50,
			nRST			=> nRST,
			TIC			=> TIC,
			SRST			=> SRST,
			DOUT			=> DIN,
			RD				=> RD,
			WE				=> WE,
			QUEUED		=> QUEUED,
			NACK			=> NACK,
			STOP			=> STOP,
			DATA_V		=> DATA_V,
			DIN			=> DOUT,
			ADR			=> ADR,
			DATA			=> DATA,
			LOAD			=> LOAD,
			COMPLETED	=> COMPLETED,
			RESCAN		=> RESCAN
		);

	-- The I2C master component to create an I2C master
	I_I2CMASTER_0 : I2CMASTER
		generic map (
			DEVICE 		=> x"68"
		)
		port map (
			MCLK			=> CLOCK_50,
			nRST			=> nRST,
			SRST			=> SRST,
			TIC			=> TIC,
			DIN			=> DIN,
			DOUT			=> DOUT,
			RD				=> RD,
			WE				=> WE,
			NACK			=> NACK,
			QUEUED		=> QUEUED,
			DATA_V		=> DATA_V,
			STOP			=> STOP,
			STATUS		=> STATUS,
			SCL_IN		=> SCL_IN,
			SCL_OUT		=> SCL_OUT,
			SDA_IN		=> SDA_IN,
			SDA_OUT		=> SDA_OUT
		);

	-- Used for creating the SCL signal
	TIC	<= counter(7) and counter(5);

	-- Sequential process --
	-- Used to generate the counter signal for the i2cmaster
	process(CLOCK_50, nRST)
	begin

		if (nRST = '0') then -- If the reset key is pressed
			counter <= (others => '0'); -- Restart the counter
		elsif rising_edge(CLOCK_50) then -- For every clock tick
			if (TIC = '1') then -- Restart the counter if it has reached the end
				counter <= (others => '0');
			else -- Add 1 to the counter
				counter <= std_logic_vector(to_unsigned(to_integer(unsigned(counter)) + 1, 8));
			end if;
		end if;

	end process;
	
	-- Sequential process --
	-- Reads DATA of the MPU6050 component and stores it in XREG_1/2
	process(CLOCK_50, nRST)
	begin

		if (nRST = '0') then
			XREG_1 <= (others => '0'); -- Set both registers back to 0
			XREG_2 <= (others => '0');
		elsif rising_edge(CLOCK_50) then -- For ever clock tick
			if (TIC = '1' and LOAD = '1') then
				if (ADR = x"0") then -- If the transaction has ended
					if (c_sensor = '1') then -- Store the sensor data in the according register, depending on which the active sensor
						XREG_1 <= DATA;
					else
						XREG_2 <= DATA;
					end if;
				end if;
			end if;
		end if;

	end process;

	-- Sequential process --
	-- Restarts the I2C data transmission and changes the addr by inverting the c_sensor signal
	process(CLOCK_50, nRST)
	begin

		if (nRST = '0') then			
			RESCAN <= '0'; -- Restart the I2C transaction
			c_sensor <= '0'; -- Set the current sensor to read to 0
		elsif rising_edge(CLOCK_50) then
			c_sensor <= c_sensor_nxt; -- Keep sensor value

			if (TIC = '1') then
				if (COMPLETED = '1') then -- If the I2C transaction is completed
					c_sensor_nxt <= not(c_sensor); -- Start reading the value of the other sensor and restart the transaction
					RESCAN <= '1';
				else
					RESCAN <= '0';
				end if;
			end if;
		end if;

	end process;

	-- Combinational process --
	-- Compares the two sensor values and turns on the vibration motor if it's larger than a given threshold
	process(XREG_1, XREG_2)
	begin

		if abs(signed(signed(XREG_1(7 downto 0)) - signed(XREG_2(7 downto 0)))) > 3 then
			GPIO_VIB	<= '1';
		else
			GPIO_VIB	<= '0';
		end if;
		
	end process;
	
	LED_R <= KEY_R;
	
	-- Assign GPIO_INV to c_sensor for changing the address
	GPIO_INV <= c_sensor;
	
	-- Assign SCL/SDA Pins
	GPIO_SCL <= 'Z' when SCL_OUT = '1' else '0';
	SCL_IN <= to_UX01(GPIO_SCL);
	
	GPIO_SDA <= 'Z' when SDA_OUT = '1' else '0';
	SDA_IN <= to_UX01(GPIO_SDA);
	
end arch;