# Sitzsensor

This project is based on the I2C Master written by tirfil 
(https://github.com/tirfil/vhdI2CMaster). 

It reads out two MPU-6050 sensors, 
compares their values and turns on a GIPO pin if the difference is above a 
definied threshold.